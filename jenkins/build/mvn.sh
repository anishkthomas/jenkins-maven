#!/bin/bash

echo "***********************"
echo "Building Jar"
echo "***********************"
WORKSPACE=/var/jenkins_home/workspace/maven-app-pipeline
echo "sudo docker run --rm -v $WORKSPACE/java-app:/app -v
$WORKSPACE/.m2:/root/.m2 -w /app maven:3-alpine"
sudo docker run --rm -v $WORKSPACE/java-app:/app -v $WORKSPACE/.m2:/root/.m2 -w /app maven:3-alpine "$@"
