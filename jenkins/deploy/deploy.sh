#!/bin/bash

echo "jenkins-maven" > /tmp/.auth
echo "$BUILD_TAG" >> /tmp/.auth
echo "$PASS" >> /tmp/.auth

scp -i /Users/anishthomas/source/play/jenkins/udemy_devops/ssh_key/remote-key /tmp/.auth jenkins@192.168.3.6:/tmp/.auth
scp -i /Users/anishthomas/source/play/jenkins/udemy_devops/ssh_key/remote-key ./jenkins/deploy/publish.sh jenkins@192.168.3.6:/tmp/publish.sh
ssh -i /Users/anishthomas/source/play/jenkins/udemy_devops/ssh_key/remote-key jenkins@192.168.3.6 "/tmp/publish.sh"