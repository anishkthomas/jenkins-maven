#!/bin/sh

echo "************************"
echo "Pushing image"
echo "************************"

IMAGE="jenkins-maven"

echo "Logging in"
docker login -u anishkthomas -p $PASS
echo "Tagging image"
docker tag $IMAGE:$BUILD_TAG anishkthomas/$IMAGE:$BUILD_TAG
echo "Pushing image"
docker push anishkthomas/$IMAGE:$BUILD_TAG