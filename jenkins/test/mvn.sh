#!/bin/bash

echo "***********************"
echo "Testing the code"
echo "***********************"

WORKSPACE=/var/jenkins_home/workspace/maven-app-pipeline
sudo docker run --rm -v $WORKSPACE/java-app:/app -v $WORKSPACE/.m2:/root/.m2 -w /app maven:3-alpine "$@"
